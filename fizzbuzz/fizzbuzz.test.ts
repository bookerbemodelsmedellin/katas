var fizzbuzz= require('../main/fizzbuzz');
    

describe('fizzbuzz solve',()=>{
    it('should return "" if n is divisible for 3',()=>{
        expect(fizzbuzz.solve(9)).toEqual("");
       
        
    })

    it('should return fizz if n is divisible by 2',()=>{
        expect(fizzbuzz.solve(4)).toEqual('fizz');
    })
})